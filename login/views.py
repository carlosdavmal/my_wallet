from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from .forms import LoginForm

# Create your views here.
@login_required
def page(request):
    template ='page.html'
    context = {'section':'dashboard'}
    if dashboard(request):

        return render(request, template, context)
def logger(request):
    
    if request.method =='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request, 
                                username=cd['username'],
                                password=cd['password']
                                )
            if user is not None:
                if user.is_active:
                    login(request,user)
                    return HttpResponse('Authenticated''succesfully')
                else:
                    return HttpResponse('Disable Account')
            else:
                return HttpResponse("Invalid login")
    else:
        form = LoginForm()
    template = 'login.html'
    context ={'form':form}
    return render(request, template, context)